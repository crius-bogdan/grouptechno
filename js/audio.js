$(document).ready(function(){
        // audio
                    // localStorage.clear()
        let myaudio = document.getElementById("myaudio");
        let musicText = $('#musicText');
        let myaudioPlayStatus;
        if(myaudio.paused){
            myaudioPlayStatus = false
            musicText.text('on')
        }else{
            myaudioPlayStatus = true
            musicText.text('off')
        }
        $('a[data-modal-btn]').on('click', function(e){
            e.preventDefault();
            let modalId = $(this).attr('href');
            if($(modalId).hasClass('showModal')){
                $('.showModal').removeClass('showModal')
            }else{
                $('.showModal').removeClass('showModal')
                $(modalId).addClass('showModal');
            }
    
            let modalUrl = $(this).attr('data-modal-btn');
            if(modalId == '#modalMusic'){
                if(myaudio.paused){
                    myaudioPlayStatus = false
                    musicText.text('on')
                    
                }else{
                    myaudioPlayStatus = true
                    musicText.text('off')
                }
            }
            if(modalId == '#modalAllPages'){
                if(modalUrl != ''){
                    $('#modalAllPagesHref').attr('href', modalUrl)
                }
            }
        })
        $('#onMusicBtn').on('click', function(e){
            e.preventDefault;
            if(myaudioPlayStatus){
                myaudio.pause();
            }else{
                myaudio.play();
            }
            $('.showModal').removeClass('showModal')
        })
        $('#offMusicBtn').on('click', function(e){
            e.preventDefault;
            if(myaudioPlayStatus){
                myaudio.play();
            }else{
                myaudio.pause();
            }
            $('.showModal').removeClass('showModal')
        })

    $('#prevAudio').on('click', function(e){
        e.preventDefault;
        let prevMusicLi = $('.playlist li[selected="selected"]').prev('li');
        let prevMusicLiSrc;
        $('.playlist li[selected="selected"]').removeAttr('selected')
        if(prevMusicLi.length > 0){
            prevMusicLiSrc = prevMusicLi.attr('audiourl');
            prevMusicLi.attr('selected', 'selected');
        }else{
            prevMusicLiSrc = $('.playlist li:last-child').attr('audiourl');
            $('.playlist li:last-child').attr('selected', 'selected');
        }
        myaudio.setAttribute('src', prevMusicLiSrc);
        myaudio.play();
    })
    $('#nextAudio').on('click', nextAudioPlayAuto)
    function nextAudioPlayAuto(){
        let nextMusicLi = $('.playlist li[selected="selected"]').next('li');
        let nextMusicLiSrc;
        $('.playlist li[selected="selected"]').removeAttr('selected')


        $('.playlist li[selected="selected"]').removeAttr('selected')
        if(nextMusicLi.length > 0){
            nextMusicLiSrc = nextMusicLi.attr('audiourl');
            nextMusicLi.attr('selected', 'selected');
        }else{
            nextMusicLiSrc = $('.playlist li:first-child').attr('audiourl');
            $('.playlist li:first-child').attr('selected', 'selected');
        }
        myaudio.setAttribute('src', nextMusicLiSrc);
        myaudio.play();
    }
    $('.nextPage').on('click', function(e){
        let currentAudio = { name: null, time: null };
        if(!myaudioPlayStatus){
            currentAudio.name = myaudio.getAttribute('src')
            currentAudio.time = myaudio.currentTime
            localStorage.setItem('currentAudio', JSON.stringify(currentAudio));
        }else{
            localStorage.clear()
        }
    })
    $(window).on('load', function(){
        let hasMusic = JSON.parse(localStorage.getItem('currentAudio'));
        if(hasMusic){
            myaudio.setAttribute('src', hasMusic.name)
            myaudio.currentTime = hasMusic.time
        }
    })
    myaudio.onended = function() {
        nextAudioPlayAuto()
    };
})