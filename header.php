<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<header class="main_header">GroupTechno </header>
    <nav class="main_nav">
        <a href="#" class="menu_btn">Files</a>
        	<?php wp_nav_menu([
			    'theme_location'  => 'header_menu',
			    'menu_class'      => 'menu_list',
                'container' => false,
	    	]);

	    	?>
 <!--        <ul class="menu_list">
            <li class="active"><a href="#" class="nextPage">Home<span>P1</span></a></li>
            <li class=""><a href="#" class="nextPage">Mobile Apps<span>P2</span></a></li>
            <li class=""><a href="#" class="nextPage">Web Dev<span>P3</span></a></li>
            <li class=""><a href="#" class="nextPage">Software Dev<span>P4</span></a></li>
            <li class=""><a href="#" class="nextPage">Blockchain<span>P5</span></a></li>
            <li class=""><a href="#" class="nextPage">Adwertising<span>P6</span></a></li>
            <li class=""><a href="#" class="nextPage">Automation<span>P7</span></a></li>
            <li><a href="#modalQuit" data-modal-btn="" class="quit">Quit <span>P8</span></a></li>
        </ul> -->
        <div class="main_nav_right">
            <a href="#modalLeva" data-modal-btn="modalLeva" class="contact_modalBtn">[Leave a message]</a>
            <ul class="social_list">
                <li>
                	<a href="<?php echo get_option( 'facebook' ); ?>" target="_blank">f</a>
                </li>
                <li>
                	<a href="<?php echo get_option( 'instagram' ); ?>" target="_blank">
                        <img src="<?php echo get_template_directory_uri().'/img/icon.svg' ?>" alt="">
                    </a>
                </li>
                <li>
                	<a href="<?php echo get_option( 'telegram' ); ?>" target="_blank">
                        <img src="<?php echo get_template_directory_uri().'/img/telegram-icon.svg' ?>" alt="">
                    </a>
                </li>
                <li>
                	<a href="<?php echo get_option( 'git' ); ?>" target="_blank" class="gitLink">Git</a>
                </li>
                <li>
                	<a href="<?php echo get_option( 'm' ); ?>" target="_blank">M</a>
                </li>
            </ul>
        </div>
    </nav>

