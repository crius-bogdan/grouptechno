<?php /* Template Name: MainTemplate */ ?>

<?php get_header(); ?>
<?php $current_url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>

<section class="content_page">
    <div class="blogs_container">
        <ul class="blogs_title">
            <li><?php the_title();?></li>
        </ul>

        <?php while ( have_posts() ) : the_post(); ?>
        <?php the_content(); ?>
        <?php endwhile; wp_reset_query(); ?>

        <div class="connect_form">
            <h4 class="form_title">leave a request for project development</h4>
            <form class="form_message" novalidate>
                <div class="form-group">
                    <label for="name">NAME</label>
                    <div class="form_quote">
                        <input type="text" id="name" name="name" class="form_input" inputmode="verbatim">
                    </div>
                </div>
                <div class="form-group">
                    <label for="email">E-MAIL </label>
                    <div class="form_quote">
                        <input type="text" id="email" name="email" class="form_input"  inputmode="verbatim">
                    </div>
                </div>
                <div class="form-group">
                    <label for="phone">PHONE</label>
                    <div class="form_quote">
                        <input type="phone" id="phone" name="phone" class="form_input" inputmode="verbatim">
                    </div>
                </div>
                <div class="form-group">
                    <label for="country">COUNTRY </label>
                    <div class="form_quote">
                        <input type="text" id="country" name="country" class="form_input" inputmode="verbatim">
                    </div>
                </div>
                <div class="form-group">
                    <label for="messenger">MESSENGER</label>
                    <div class="form_quote">
                        <input type="text" id="messenger" name="messenger" class="form_input" inputmode="verbatim" >
                    </div>
                </div>
                <div class="form-group">
                    <button type="button" id="send_request" class="btn_request">[Press to leave a request]</button>
                </div>
            </form>
        </div>
    </div>
</section>

  <section class="breadcrumb_section">
        <div class="breadcrumb_container">
            <div class="search_frame">
                <ul class="breadcrumbs">
                    <li>
                        <a href="/"><span>GT:</span></a>
                    </li>
                    <li class="active">
                        <span><?php echo rtrim(str_replace('/','\\',$_SERVER['REQUEST_URI']),'\\');?></span>
                    </li>
                </ul>
            </div>
        </div>
    </section>
  <!-- footer -->

    <!-- section-content-end -->
    <section class="breadcrumb_section">
        <div class="breadcrumb_container">
            <div class="search_frame">
                <ul class="breadcrumbs">
                    <li><a href="\">GT:\></a></li>
                    <li>
                        <form action="<?php $current_url; ?>" method="POST">
                            <input type="text" id="search" name="mysearch" class="input_style" placeholder="_" autocomplete="off">
                        </form>
                    </li>
                    <?php
                        $value = $_POST['mysearch'];
                        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                            if ($value) {
                                $page = get_page_by_title($value);
                                if ($page){
                                    echo '<script>window.location.href = "'.get_permalink($page->ID).'"</script>';
                                }
                                if ($post){
                                    $args = array("post_type" => "blogs", "name" => $value);
                                    $query = get_posts( $args );
                                    foreach ($query as $key => $value) {
                                        echo '<script>window.location.href = "'.get_permalink($value->ID).'"</script>';
                                    }
                                }
                            }
                        }
                    ?>
                </ul>
            </div>
        </div>
    </section>

<?php get_footer(); ?>
