<?php
get_header();
?>
<?php $current_url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>
	<?php if (have_posts()) : ?>
		<section class="content_page">
	        <div class="blogs_container">
				<?php while (have_posts()) : the_post(); ?>
		            <ul class="blogs_title">
		                <li>
		                    <a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a>
		                </li>
		            </ul>
				<?php endwhile;
					else :
					get_template_part( 'template-parts/content', 'none' );
				endif; ?>
	        </div>
	    </section>

    <section class="breadcrumb_section">
        <div class="breadcrumb_container">
            <div class="search_frame">
                <ul class="breadcrumbs">
                    <li>
                        <a href="/"><span>GT:</span></a>
                    </li>
                    <li class="active">
                        <a href="<?php echo $current_url; ?>">
                            <span><?php echo rtrim(str_replace('/','\\',$_SERVER['REQUEST_URI']),'\\');?></span>
                        </a>
                    </li>
                </ul>        
            </div>
        </div>
    </section>
  <!-- footer -->

    <!-- section-content-end -->
    <section class="breadcrumb_section">
        <div class="breadcrumb_container">
            <div class="search_frame">
                <ul class="breadcrumbs">
                    <li><a href="\">GT:\></a></li>
                    <li>
                        <form action="<?php $current_url; ?>" method="POST">
                            <input type="text" id="search" name="mysearch" class="input_style" placeholder="_" autocomplete="off">
                        </form>
                    </li>
                    <?php 
                        $value = $_POST['mysearch']; 
                        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	                        if ($value) {
	                            $page = get_page_by_title($value);
	                            if ($page){
	                                echo '<script>window.location.href = "'.get_permalink($page->ID).'"</script>';
	                            }
	                            if ($post){
	                                $args = array("post_type" => "blogs", "name" => $value);
	                                $query = get_posts( $args );
	                                foreach ($query as $key => $value) {
	                                    echo '<script>window.location.href = "'.get_permalink($value->ID).'"</script>';
	                                }
	                            }
	                        }
						}
                    ?>
                </ul>
            </div>
        </div>
    </section>
<?php
get_footer();
