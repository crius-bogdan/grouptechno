<?php /* Template Name: EggsTemplate */ ?>

<?php get_header(); ?>
<section class="content_page">
    <div class="eggs_container">
        <div class="eggs_container_text">
            <p>Wake up, Neo...</p>
            <p>The Matrix has you..</p>
            <p>Follow the white rabbit.</p>
            <p> &nbsp;</p>
            <p> &nbsp;</p>
            <p> Knock, knock, Neo.</p>
        </div>
        <input type="text" class="input_style" placeholder="_" value="" id="eggsInput">
    </div>
</section>

<!-- section-content-end -->
<section class="breadcrumb_section">
    <div class="breadcrumb_container">
        <div class="search_frame">
            <ul class="breadcrumbs">
                <?php $current_url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>
                <li><a href="\">GT:\></a></li>
                <li>
                    <form action="<?php $current_url; ?>" method="POST">
                        <input type="text" id="search" name="mysearch" class="input_style" placeholder="_" autocomplete="off">
                    </form>
                </li>
                <?php 
                    $value = $_POST['mysearch']; 
                        if ($_SERVER['REQUEST_METHOD'] === 'POST'){
                            if ($value) {
                                $page = get_page_by_title($value);
                                if ($page){
                                    echo '<script>window.location.href = "'.get_permalink($page->ID).'"</script>';
                                }
                                if ($post){
                                    $args = array("post_type" => "blogs", "name" => $value);
                                    $query = get_posts( $args );
                                    foreach ($query as $key => $value) {
                                        echo '<script>window.location.href = "'.get_permalink($value->ID).'"</script>';
                                    }
                                }
                            }
                        }
                ?>
            </ul>
        </div>
    </div>
</section>

<?php get_footer(); ?>