<?php
get_header();
?>

    <section class="services_block content_page">
        <?php
        $args = array(
            'sort_order'   => 'ASC',
            'sort_column'  => 'ID',
            'meta_value' => 'mainTemplate.php'
        );
        ?>
        <?php
        $pages = get_pages($args);
        foreach ($pages as $page){?>
            <?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($page->ID), 'thumbnail_size' ); ?>
            <a href="#modalAllPages" data-modal-btn="<?php echo get_page_link( $page->ID ); ?>" class="services_item">
                <div class="services_item_img"><img src="<?php echo $src[0]; ?>" alt=""></div>
                <h6><?php echo $page->post_title; ?></h6>
            </a>
        <?php }
        ?>
    </section>
    <div class="beforeFooter">
        <p>GT GroupTechno 2019 “The future is near”</p>
        <p>We develop the most advanced technological solutions</p>
    </div>


    <!-- footer -->

    <!-- section-content-end -->
    <section class="breadcrumb_section">
        <div class="breadcrumb_container">
            <div class="search_frame">
                <ul class="breadcrumbs">
                    <?php $current_url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>
                    <li><a href="\">GT:\></a></li>
                    <li>
                        <form action="<?php $current_url; ?>" method="POST">
                            <input type="text" id="search" name="mysearch" class="input_style" placeholder="_" autocomplete="off">
                        </form>
                    </li>
                    <?php
                    $value = $_POST['mysearch'];
                    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                        if ($value) {
                            $page = get_page_by_title($value);
                            if ($page){
                                echo '<script>window.location.href = "'.get_permalink($page->ID).'"</script>';
                            }
                            if ($post){
                                $args = array("post_type" => "blogs", "name" => $value);
                                $query = get_posts( $args );
                                foreach ($query as $key => $value) {
                                    echo '<script>window.location.href = "'.get_permalink($value->ID).'"</script>';
                                }
                            }
                        }
                    }
                    ?>
                </ul>
            </div>
        </div>
    </section>

    <section class="loader" id="preloader" style="display: none;">
        <div class="loader_container">
            <div class="group_techno_cantainer">
                <div class="group_techno_text">
                    <p>GROUPTECHNO</p>
                </div>
                <div class="group_techno_number">
                    <span>3.12</span>
                    <span>128K</span>
                    <span>19</span>
                </div>
            </div>
            <div class="group_techno_welcome mt-50">
                <p>Welcome to Grouptechno’s Web implementation 2019 - 09 - 12</p>
            </div>
            <div class="group_techno_webdesign">
                <p>Web Design Shell(GT) Version 3.12</p>
                <p>Copyright 2010, 11, 12, 13, 14, 15, 16, 17, 18, 19 Grouptechno Corp</p>
            </div>
            <div class="group_techno_version mt-50">
                <p>GT(R) Grouptechno(R) Version 1.01</p>
                <p class="copyright_year">(C)Copyright 2010 - 2019</p>
            </div>
            <div class="group_techno_device_driver">
                <p>Device Driver Version 9.02b OK</p>
            </div>
            <div class="group_techno_startup mt-50">
                <p>GT(C)2019 Web Shell Starting up</p>
            </div>
            <div class="group_techno_loaded mt-50">
                <p>Loaded consuming 2381541 bytes</p>
            </div>
            <div class="group_techno_loading_files mt-50">
                <p>Loading data</p>
            </div>
        </div>
    </section>
<?php
get_footer();
