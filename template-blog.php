<?php /*
 * Template Name: Blogs
 * Template Post Type: blogs
 */ ?>
<?php get_header();?>
<?php $current_url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>

<?php if (have_posts()) : while (have_posts()) : the_post();?>
    <section class="content_page">
        <div class="blogs_container">
            <ul class="blogs_title">
                <li><?php the_title();?></li>
            </ul>
            <div class="blogs_container_content">
                <?php the_content(); ?>
            </div>
        </div>
    </section>
<?php endwhile; endif;?>

    <section class="breadcrumb_section">
        <div class="breadcrumb_container">
            <div class="search_frame">
                <ul class="breadcrumbs">
                    <li>
                        <a href="/"><span>GT:</span></a>
                    </li>
                    <li>
                        <a href="/blogs"><span>\blogs</span></a>
                    </li>
                    <li class="active">
                        <?php $singUrl = explode('\\',rtrim(str_replace('/','\\',$_SERVER['REQUEST_URI']),'\\')); ?>
                        <span><?php echo '\\'.$singUrl[2];?></span>
                    </li>
                </ul>
            </div>
        </div>
    </section>

    <!-- section-content-end -->
    <section class="breadcrumb_section">
        <div class="breadcrumb_container">
            <div class="search_frame">
                <ul class="breadcrumbs">
                    <li><a href="\">GT:\></a></li>
                    <li>
                        <form action="<?php $current_url; ?>" method="POST">
                            <input type="text" id="search" name="mysearch" class="input_style" placeholder="_" autocomplete="off">
                        </form>
                    </li>
                    <?php
                    $value = $_POST['mysearch'];
                    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                        if ($value) {
                            $page = get_page_by_title($value);
                            if ($page){
                                echo '<script>window.location.href = "'.get_permalink($page->ID).'"</script>';
                            }
                            if ($post){
                                $args = array("post_type" => "blogs", "name" => $value);
                                $query = get_posts( $args );
                                foreach ($query as $key => $value) {
                                    echo '<script>window.location.href = "'.get_permalink($value->ID).'"</script>';
                                }
                            }
                        }
                    }
                    ?>
                </ul>
            </div>
        </div>
    </section>

<?php get_footer();?>
